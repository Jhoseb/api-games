﻿using JuegosAPI.Controllers.Authentication;
using JuegosAPI.Models;
using JuegosAPI.Services.Authentication;
using JuegosAPI.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace JuegosAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = UserDataStorage.CurrentUserDataStorage.Users;
            var responseData = new
            {
                status = "success",
                message = "Data retrieved successfully",
                data = users
            };
            return Ok(responseData);
        }

        [HttpGet("{id}")]
        public IActionResult GetOne(Guid id)
        {
            var user = UserDataStorage.CurrentUserDataStorage.Users!.FirstOrDefault(u => u.Id == id);
            if (user != null)
            {
                var responseData = new
                {
                    status = "success",
                    message = "Data retrieved successfully",
                    data = user
                };
                return Ok(responseData);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] User newUser)
        {
            var existingUser =
                UserDataStorage.CurrentUserDataStorage.Users!.FirstOrDefault(
                    u => u.Username == newUser.Username);
            if (existingUser != null)
            {
                return Conflict(ErrorMessages.DuplicateUser);
            }

            if (!PasswordValidator.IsPasswordValid(newUser.Password))
            {
                return BadRequest(ErrorMessages.InvalidPassword);
            }

            newUser.Id = Guid.NewGuid();

            newUser.Password = PasswordHasher.HashPassword(newUser.Password);

            UserDataStorage.CurrentUserDataStorage.Users.Add(newUser);

            return CreatedAtAction(nameof(GetOne), new { id = newUser.Id }, newUser);
        }

        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] User updatedUser)
        {
            var existingUser =
                UserDataStorage.CurrentUserDataStorage.Users.FirstOrDefault(u => u.Id == id);
            if (existingUser == null)
            {
                return NotFound();
            }

            if (!PasswordValidator.IsPasswordValid(updatedUser.Password))
            {
                return BadRequest(ErrorMessages.InvalidPassword);
            }

            if (existingUser.Password != updatedUser.Password)
            {
                updatedUser.Password = PasswordHasher.HashPassword(updatedUser.Password);
            }

            existingUser.Username = updatedUser.Username;
            existingUser.Password = updatedUser.Password;
            existingUser.Role = updatedUser.Role;

            return Ok(existingUser);
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            var userToRemove =
                UserDataStorage.CurrentUserDataStorage.Users.FirstOrDefault(u => u.Id == id);
            if (userToRemove != null)
            {
                UserDataStorage.CurrentUserDataStorage.Users.Remove(userToRemove);
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
