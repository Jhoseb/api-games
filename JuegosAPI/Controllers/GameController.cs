﻿using JuegosAPI.Models;
using JuegosAPI.Services;
using JuegosAPI.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JuegosAPI.Controllers
{
    [ApiController]
    [Route("api/videogames")]
    [Authorize(Roles = UserRole.Admin)]
    public class GameController : ControllerBase
    {
        [HttpPost]
        public ActionResult<Videogame> PostVideogame([FromBody] VideogameInsert videogameInsert)
        {
            if (videogameInsert == null || string.IsNullOrWhiteSpace(videogameInsert.Name) ||
                !VideogameValidator.IsValidPlatform(videogameInsert.Platform) ||
                !VideogameValidator.IsValidPrice(videogameInsert.Price))
            {
                return BadRequest(ErrorMessages.InvalidData);
            }

            if (!VideogameValidator.IsUniqueName(videogameInsert.Name,
                                                 VideogameGameDataStorage.CurrentGameDataStorage.Videogames))
            {
                return BadRequest(ErrorMessages.DuplicateName);
            }

            var newVideogame = new Videogame()
            {
                Id = Guid.NewGuid(),
                Name = videogameInsert.Name,
                Platform = videogameInsert.Platform,
                Price = videogameInsert.Price
            };
            VideogameGameDataStorage.CurrentGameDataStorage.Videogames.Add(newVideogame);
            return newVideogame;
        }

        [HttpDelete("{videogameId}")]
        public ActionResult DeleteVideogame([FromRoute] Guid videogameId)
        {
            if (VideogameGameDataStorage.CurrentGameDataStorage.Videogames != null)
            {
                var videogame =
                    VideogameGameDataStorage.CurrentGameDataStorage.Videogames.FirstOrDefault(
                        x => x.Id == videogameId);
                if (videogame == null)
                    return NotFound(ErrorMessages.NotFound);
                VideogameGameDataStorage.CurrentGameDataStorage.Videogames.Remove(videogame);
            }

            return StatusCode(StatusCodes.Status200OK);
        }

        [HttpGet]
        [Authorize]
        public ActionResult<IEnumerable<Videogame>> GetVideogames()
        {
            var videogames = VideogameGameDataStorage.CurrentGameDataStorage.Videogames;
            var responseData = new
            {
                status = "success",
                message = "Data retrieved successfully",
                data = videogames
            };
            return Ok(responseData);
        }

        [HttpGet("{videogameId}")]
        public ActionResult<Videogame> GetVideogame([FromRoute] Guid videogameId)
        {
            var videogame =
                VideogameGameDataStorage.CurrentGameDataStorage.Videogames!.FirstOrDefault(
                    x => x.Id == videogameId);
            if (videogame == null)
                return NotFound(ErrorMessages.NotFound);

            var responseData = new
            {
                status = "success",
                message = "Data retrieved successfully",
                data = videogame
            };
            return Ok(responseData);
        }

        [HttpPut("{videogameId}")]
        [Authorize(Roles = UserRole.Admin)]
        public ActionResult<Videogame> UpdateVideogame([FromRoute] Guid videogameId,
                                                       [FromBody] VideogameInsert videogameInsert)
        {
            if (videogameInsert == null || string.IsNullOrWhiteSpace(videogameInsert.Name) ||
                !VideogameValidator.IsValidPlatform(videogameInsert.Platform) ||
                !VideogameValidator.IsValidPrice(videogameInsert.Price))
            {
                return BadRequest(ErrorMessages.InvalidData);
            }

            var videogame =
                VideogameGameDataStorage.CurrentGameDataStorage.Videogames.FirstOrDefault(
                    x => x.Id == videogameId);
            if (videogame == null)
                return NotFound(ErrorMessages.NotFound);
            videogame.Name = videogameInsert.Name;
            videogame.Platform = videogameInsert.Platform;
            videogame.Price = videogameInsert.Price;
            return Ok(videogame);
        }
    }


}
