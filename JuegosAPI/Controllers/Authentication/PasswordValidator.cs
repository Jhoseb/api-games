﻿namespace JuegosAPI.Controllers.Authentication
{
    public class PasswordValidator
    {
        public static bool IsPasswordValid(string password)
        {
            if (password.Length < 8)
            {
                return false;
            }

            if (!ContainsNumericCharacter(password))
            {
                return false;
            }

            return true;
        }

        private static bool ContainsNumericCharacter(string password)
        {
            return password.Any(char.IsDigit);
        }
    }
}
