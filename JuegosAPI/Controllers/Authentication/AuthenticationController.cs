﻿using JuegosAPI.Models;
using JuegosAPI.Services.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace JuegosAPI.Controllers.Authentication
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly string? _secretKey;

        public AuthenticationController(IConfiguration configuration)
        {
            _secretKey = configuration.GetSection("settings").GetSection("secretKey").ToString();
        }

        [HttpPost]
        [Route("validation")]
        public IActionResult Validate([FromBody] User request)
        {
            var user =
                UserDataStorage.CurrentUserDataStorage.Users?.Find(u => u.Username == request.Username);
            if (user != null && PasswordHasher.VerifyPassword(request.Password, user.Password))
            {
                var token = GenerateToken(user);
                return StatusCode(StatusCodes.Status200OK, new { token });
            }
            else
            {
                return StatusCode(StatusCodes.Status401Unauthorized, new { token = "" });
            }
        }

        private string GenerateToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_secretKey);

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Username),
                new Claim(ClaimTypes.Role, user.Role)
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials =
                    new SigningCredentials(new SymmetricSecurityKey(key),
                                           SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
