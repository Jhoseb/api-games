﻿using JuegosAPI.Validation;
using System.ComponentModel.DataAnnotations;

namespace JuegosAPI.Models
{
    public class User
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required(ErrorMessage = ErrorMessages.EmailRequired)]
        [MaxLength(100)]
        public string Username { get; set; }

        [Required(ErrorMessage = ErrorMessages.PasswordRequired)]
        [MinLength(8)]
        [MaxLength(100)]
        public string Password { get; set; }

        [Required(ErrorMessage = ErrorMessages.RoleRequired)]
        public string Role { get; set; }
    }
}
