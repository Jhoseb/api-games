namespace JuegosAPI.Models
{
    public static class PlatformConstants
    {
        public const string Xbox = "Xbox";
        public const string PlayStation = "PlayStation";
    }
}