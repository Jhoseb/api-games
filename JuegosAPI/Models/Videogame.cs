using System;
using System.ComponentModel.DataAnnotations;
using JuegosAPI.Validation;

namespace JuegosAPI.Models
{
    public class Videogame
    {
        public Guid Id { get; set; } = Guid.NewGuid(); // Asignar un nuevo GUID por defecto al crear una nueva instancia

        [Required(ErrorMessage = ErrorMessages.NameRequired)]
        [MaxLength(100, ErrorMessage = ErrorMessages.NameMaxLength)]
        public string Name { get; set; } = string.Empty;

        [Required(ErrorMessage = ErrorMessages.PlatformRequired)]
        [MaxLength(100)]
        public string Platform { get; set; }

        [Required(ErrorMessage = ErrorMessages.PriceRequired)]
        [Range(0, double.MaxValue, ErrorMessage = ErrorMessages.PriceNegative)]
        public decimal Price { get; set; }
    }
}