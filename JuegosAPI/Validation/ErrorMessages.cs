namespace JuegosAPI.Validation
{
    public static class ErrorMessages
    {
        public const string NameRequired = "Name is required";
        public const string NameMaxLength = "Only 100 characters allowed";
        public const string PlatformRequired = "Platform is required";
        public const string PriceRequired = "Price is required";
        public const string PriceNegative = "Price cannot be negative";
        public const string DuplicateName = "A video game with the same name already exists.";
        public const string InvalidData = "Invalid data provided.";
        public const string NotFound = "Videogame not found.";
        public const string InvalidPlatform = "Invalid platform provided.";
        public const string InvalidRole = "Invalid role provided.";
        public const string DuplicateUser = "User already exists.";
        public const string InvalidPassword = "Password needs at least one number.";
        public const string ShortPassword = "The password must be longer than 8 characters.";
        public const string EmailRequired = "Username is required.";
        public const string PasswordRequired = "Password is required.";
        public const string RoleRequired = "Role is required.";
    }
}