using System.Collections.Generic;
using System.Linq;
using JuegosAPI.Models;

namespace JuegosAPI.Validation
{
    public static class VideogameValidator
    {
        public static bool IsUniqueName(string name, IEnumerable<Videogame> videogames)
        {
            return !videogames.Any(v => v.Name.Equals(name, System.StringComparison.OrdinalIgnoreCase));
        }

        public static bool IsValidPlatform(string platform)
        {
            return platform == PlatformConstants.Xbox || platform == PlatformConstants.PlayStation;
        }

        public static bool IsValidPrice(decimal price)
        {
            return price >= 0;
        }
    }
}