﻿using JuegosAPI.Models;

namespace JuegosAPI.Services.Authentication
{
    public class UserDataStorage
    {
        public List<User>? Users { get; }

        public static UserDataStorage CurrentUserDataStorage { get; } = new UserDataStorage();

        public UserDataStorage()
        {
            Users = new List<User>()
            {
                new User
                {
                    Id = new Guid("3F2504E0-4F89-11D3-9A0C-0305E82C3303"),
                    Username = "Jose.Moya@jala.universiy",
                    Password = "12345678",
                    Role = "Admin"
                },
                new User
                {
                    Id = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                    Username = "string",
                    Password = "Afeksh21",
                    Role = "Admin"
                }
            };
        }
    }
}

