using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JuegosAPI.Models;

namespace JuegosAPI.Services
{
    public class VideogameGameDataStorage
    {
        public List<Videogame>? Videogames { get; set; }

        public static VideogameGameDataStorage CurrentGameDataStorage { get; } = new VideogameGameDataStorage();

        public VideogameGameDataStorage()
        {
            Videogames = new List<Videogame>()
            {
                new Videogame
                {
                    Id = Guid.NewGuid(),
                    Name = "Microsoft Flight Simulator",
                    Platform = PlatformConstants.Xbox,
                    Price = 150
                },
                new Videogame
                {
                    Id = Guid.NewGuid(),
                    Name = "Call of Duty: Black Ops Cold War",
                    Platform = PlatformConstants.PlayStation,
                    Price = 60
                },
                new Videogame
                {
                    Id = Guid.NewGuid(),
                    Name = "Spider-Man: Miles Morales",
                    Platform = PlatformConstants.PlayStation,
                    Price = 50
                },
                new Videogame
                {
                    Id = Guid.NewGuid(),
                    Name = "Gears Tactics",
                    Platform = PlatformConstants.Xbox,
                    Price = 40
                }
            };
        }
    }
}
