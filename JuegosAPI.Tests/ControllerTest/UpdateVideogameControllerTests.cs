using JuegosAPI.Controllers;
using JuegosAPI.Models;
using JuegosAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace JuegosAPI.Tests.Controllers
{
    public class UpdateVideogameControllerTests
    {
        [Fact]
        public void supports_updating_multiple_fields_at_once()
        {
            // Arrange
            var controller = new GameController();
            var videogameId = Guid.NewGuid();
            var existingVideogame = new Videogame
            {
                Id = videogameId,
                Name = "Old Game",
                Platform = "PlayStation",
                Price = 49
            };
            VideogameGameDataStorage.CurrentGameDataStorage.Videogames.Add(existingVideogame);
            var videogameInsert = new VideogameInsert
            {
                Name = "New Game",
                Platform = "Xbox",
                Price = 59
            };

            // Act
            var result = controller.UpdateVideogame(videogameId, videogameInsert);

            // Assert
            Assert.Equal("New Game", existingVideogame.Name);
            Assert.Equal("Xbox", existingVideogame.Platform);
            Assert.Equal(59, existingVideogame.Price);
            Assert.IsType<OkObjectResult>(result.Result);
        }

        [Fact]
        public void UpdateVideogame_InvalidData_ReturnsBadRequest()
        {
            // Arrange
            var controller = new GameController();
            var videogameId = Guid.NewGuid();
            var videogameInsert = new VideogameInsert(); 

            // Act
            var result = controller.UpdateVideogame(videogameId, videogameInsert);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public void UpdateVideogame_NonexistentId_ReturnsNotFound()
        {
            // Arrange
            var controller = new GameController();
            var videogameId = Guid.NewGuid(); 
            var videogameInsert = new VideogameInsert
            {
                Name = "New Game",
                Platform = "Xbox",
                Price = 50
            };

            // Act
            var result = controller.UpdateVideogame(videogameId, videogameInsert);

            // Assert
            Assert.IsType<NotFoundObjectResult>(result.Result);
        }
        
        [Fact]
        public void returns_bad_request_response_when_platform_is_invalid()
        {
            // Arrange
            var controller = new GameController();
            var videogameId = Guid.NewGuid();
            var videogameInsert = new VideogameInsert
            {
                Name = "Test Game",
                Platform = "InvalidPlatform",
                Price = 59
            };

            // Act
            var result = controller.UpdateVideogame(videogameId, videogameInsert);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }
        
        [Fact]
        public void returns_bad_request_response_when_price_is_invalid()
        {
            // Arrange
            var controller = new GameController();
            var videogameId = Guid.NewGuid();
            var videogameInsert = new VideogameInsert
            {
                Name = "Test Game",
                Platform = "Xbox",
                Price = -10
            };

            // Act
            var result = controller.UpdateVideogame(videogameId, videogameInsert);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }
        
        [Fact]
        public void returns_bad_request_response_when_name_is_null_or_whitespace()
        {
            // Arrange
            var videogameId = Guid.NewGuid();
            var videogameInsert = new VideogameInsert
            {
                Name = null,
                Platform = "Xbox",
                Price = 59
            };
            var controller = new GameController();

            // Act
            var response = controller.UpdateVideogame(videogameId, videogameInsert);

            // Assert
            Assert.IsType<BadRequestObjectResult>(response.Result);
        }
    }
}