using JuegosAPI.Controllers;
using JuegosAPI.Models;
using JuegosAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace JuegosAPI.Tests;

public class CreateVideogamesControllerTest
{
    [Fact]
    public void should_create_videogame_with_valid_data()
    {
        // Arrange
        var videogameInsert = new VideogameInsert()
        {
            Name = "Test Game",
            Platform = "Xbox",
            Price = 59
        };

        var controller = new GameController();

        // Act
        var result = controller.PostVideogame(videogameInsert);

        // Assert
        Assert.NotNull(result);
        Assert.IsType<ActionResult<Videogame>>(result);
        Assert.Equal("Test Game", result.Value.Name);
        Assert.Equal("Xbox", result.Value.Platform);
        Assert.Equal(59, result.Value.Price);
    }
    
    [Fact]
    public void PostVideogame_DuplicateName_ReturnsBadRequest()
    {
        // Arrange
        var controller = new GameController();
        var existingVideogameInsert = new VideogameInsert
        {
            Name = "Gears Tactics", 
            Platform = "Xbox",
            Price = 50
        };

        // Act
        var result = controller.PostVideogame(existingVideogameInsert);

        // Assert
        Assert.IsType<BadRequestObjectResult>(result.Result);
    }
    
    [Fact]
    public void PostVideogame_InvalidData_NullFields_ReturnsBadRequest()
    {
        // Arrange
        var controller = new GameController();
        var invalidVideogameInsert = new VideogameInsert();

        // Act
        var result = controller.PostVideogame(invalidVideogameInsert);

        // Assert
        Assert.IsType<BadRequestObjectResult>(result.Result);
    }
    
    [Fact]
    public void PostVideogame_InvalidData_NegativePrice_ReturnsBadRequest()
    {
        // Arrange
        var controller = new GameController();
        var invalidVideogameInsert = new VideogameInsert
        {
            Name = "New Game",
            Platform = "Xbox",
            Price = -50 
        };

        // Act
        var result = controller.PostVideogame(invalidVideogameInsert);

        // Assert
        Assert.IsType<BadRequestObjectResult>(result.Result);
    }

    [Fact]
    public void PostVideogame_InvalidData_InvalidPlatform_ReturnsBadRequest()
    {
        // Arrange
        var controller = new GameController();
        var invalidVideogameInsert = new VideogameInsert
        {
            Name = "New Game",
            Platform = "InvalidPlatform", 
            Price = 50
        };

        // Act
        var result = controller.PostVideogame(invalidVideogameInsert);

        // Assert
        Assert.IsType<BadRequestObjectResult>(result.Result);
    }
    
}