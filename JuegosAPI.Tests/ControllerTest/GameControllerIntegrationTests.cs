using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using Xunit.Abstractions;

namespace JuegosAPI.Tests.ControllerTest
{
    public class GameControllerIntegrationTests
    {
        
        private readonly ITestOutputHelper _testOutputHelper;
        public readonly HttpClient HttpClient;
        private string? _token; // Almacenar el token para uso posterior

        public GameControllerIntegrationTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            HttpClient = new HttpClient
            {
                BaseAddress = new Uri("http://localhost:5259")
            };
        }

        [Fact]
        public async Task CreateTwoVideogames_And_VerifyData()
        {
            // Paso 1: Crear usuario y obtener el token
            await CreateUserAndRetrieveToken();

            // Paso 2: Configurar el token para la primera solicitud protegida
            HttpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _token);

            // Crear el primer videojuego
            var videogame1Json = JsonSerializer.Serialize(new
            {
                Name = "Test Game 1",
                Platform = "Xbox",
                Price = 50
            });

            var createGame1Response = await HttpClient.PostAsync(
                "/api/videogames",
                new StringContent(videogame1Json, Encoding.UTF8, "application/json")
            );
            createGame1Response.EnsureSuccessStatusCode(); // Asegura que la solicitud fue exitosa

            // Configurar el token para la segunda solicitud protegida
            HttpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _token);

            // Crear el segundo videojuego
            var videogame2Json = JsonSerializer.Serialize(new
            {
                Name = "Test Game 2",
                Platform = "PlayStation",
                Price = 60
            });

            var createGame2Response = await HttpClient.PostAsync(
                "/api/videogames",
                new StringContent(videogame2Json, Encoding.UTF8, "application/json")
            );
            createGame2Response.EnsureSuccessStatusCode(); // Asegura que la solicitud fue exitosa

            // Configurar el token para la solicitud para obtener videojuegos
            HttpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _token);

            // Obtener todos los videojuegos
            var getVideogamesResponse = await HttpClient.GetAsync("/api/videogames");
            getVideogamesResponse.EnsureSuccessStatusCode();

            var videogamesContent = await getVideogamesResponse.Content.ReadAsStringAsync();
            // Imprime el contenido completo para verificar qué se está devolviendo
            _testOutputHelper.WriteLine($"Contenido de la respuesta de 'videogames': {videogamesContent}");

            // Deserializa y verifica si es nulo
            var videogames = JsonSerializer.Deserialize<VideogameListResponse>(videogamesContent)?.Data;

            if (videogames != null)
            {
                Assert.Equal(2, videogames.Count);

                var gameNames = videogames.Select(vg => vg.Name).ToList();
                Assert.Contains("Test Game 1", gameNames);
                Assert.Contains("Test Game 2", gameNames);
            }
        }

        private async Task CreateUserAndRetrieveToken()
        {
            var userJson = JsonSerializer.Serialize(new
            {
                Username = "testuser@example.com",
                Password = "Password123!",
                Role = "Admin"
            });

            var createUserResponse = await HttpClient.PostAsync(
                "/api/User",
                new StringContent(userJson, Encoding.UTF8, "application/json")
            );
            createUserResponse.EnsureSuccessStatusCode(); // Asegura que el usuario se creó con éxito

            // Autenticar para obtener el token JWT
            var authJson = JsonSerializer.Serialize(new
            {
                Username = "testuser@example.com",
                Password = "Password123!",
                Role = "Admin"
            });

            var authResponse = await HttpClient.PostAsync(
                "/api/Authentication/validation",
                new StringContent(authJson, Encoding.UTF8, "application/json")
            );
            authResponse.EnsureSuccessStatusCode();

            var authResponseContent = await authResponse.Content.ReadAsStringAsync();

            using var jsonDocument = JsonDocument.Parse(authResponseContent);
            if (jsonDocument.RootElement.TryGetProperty("token", out var tokenElement))
            {
                _token = tokenElement.GetString(); // Extrae el token
                _testOutputHelper.WriteLine($"Token obtenido: {_token}");
            }
            else
            {
                throw new Exception("Token no encontrado en la respuesta de autenticación.");
            }
        }
        
        public class TokenResponse
        {
            public string? Token { get; set; }
        }

        public class Videogame
        {
            public string? Name { get; set; }
            public string? Platform { get; set; }
            public decimal Price { get; set; }
        }

        public class VideogameListResponse
        {
            public List<Videogame>? Data { get; set; }
        }
    }
}
